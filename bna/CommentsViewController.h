//
//  CommentsViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 11/4/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int post_id;
@property (weak, nonatomic) IBOutlet UIView *loading;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UITextField *txtComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeightConstraint;

@end
