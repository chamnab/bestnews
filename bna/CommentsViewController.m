//
//  CommentsViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 11/4/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "CommentsViewController.h"
#import "CommentTableViewCell.h"
#import "NSDate+NVTimeAgo.h"
#import "Common.h"
#import "AppDelegate.h"

@interface CommentsViewController (){
    NSMutableArray *commentList;
    NSUserDefaults *defaults;
    AppDelegate *appDelegate;
    UIRefreshControl *refreshControl;
    BOOL isRefreshed;
}

@end

@implementation CommentsViewController

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont fontWithName:[Common getRegularFont] size:15.0f]};
    
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        self.title = @"មតិ";
    }else{
        self.title = @"Comments";
    }
    
    self.tableView.estimatedRowHeight = 110.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    defaults  = [NSUserDefaults standardUserDefaults];
    appDelegate = [AppDelegate appDelegate];
    
    commentList = [NSMutableArray array];
    
    self.lblStatus.hidden = YES;
    self.loading.hidden = NO;
    [self getComment];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    if(![defaults objectForKey:@"name"]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"BN Khmer" message:([[defaults objectForKey:@"lang"]  isEqual: @"km"]) ? @"សូមបញ្ចូលឈ្មេាះរបស់អ្នក" : @"Please input your nickname" preferredStyle:UIAlertControllerStyleAlert];
        if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
            [alert addAction:[UIAlertAction actionWithTitle:@"រក្សាទុក" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [defaults setObject:alert.textFields[0].text forKey:@"name"];
                [defaults synchronize];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"ចាកចេញ" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                [defaults setObject:alert.textFields[0].text forKey:@"ភ្ញៀវ"];
                [defaults synchronize];
            }]];
        }else{
            [alert addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [defaults setObject:alert.textFields[0].text forKey:@"name"];
                [defaults synchronize];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
                [defaults setObject:alert.textFields[0].text forKey:@"Guest"];
                [defaults synchronize];
            }]];
        }
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                textField.placeholder = @"បញ្ចូលឈ្មេាះរបស់អ្នក...";
            }else{
                textField.placeholder = @"Input your nickname...";
            }
            
            textField.secureTextEntry = NO;
        }];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void) getComment{
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:[NSString stringWithFormat:@"comment/%d",self.post_id]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getComment];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSLog(@"%@",payload);
                commentList = payload[@"result"];
                if(commentList.count == 0){
                    self.lblStatus.hidden = NO;
                    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                        self.lblStatus.text = @"គ្មានមតិ";
                    }else{
                        self.lblStatus.text = @"No Comments";
                    }
                    [self.lblStatus setFont:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                    
                    self.tableView.hidden = YES;
                    self.loading.hidden = YES;
                }else{
                    [self.tableView reloadData];
                    self.loading.hidden = YES;
                }
                
            });
        }
        
    }];
    [postSession resume];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 50;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return commentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.lblName.text = commentList[indexPath.row][@"comment_author"];
    [cell.lblName setFont:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
    NSDate *date = [formatter dateFromString:commentList[indexPath.row][@"comment_date"]];
    NSString *ago = [date formattedAsTimeAgo];
    cell.lblDate.text = ago;
    
    cell.lblText.text = commentList[indexPath.row][@"comment_content"];
    [cell.lblText setFont:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
    
    return  cell;
}

#pragma mark - Keyboard
- (void)keyboardDidShow: (NSNotification *) notification{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    self.bottomHeightConstraint.constant = CGRectGetHeight(keyboardFrame);
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomHeightConstraint.constant = 0;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtComment resignFirstResponder];

    [self insertComment:self.txtComment.text];
    self.txtComment.text = @"";
    
    return NO;
}

- (void) insertComment:(NSString *)text{
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:[NSNumber numberWithInt:self.post_id] forKey:@"post_id"];
    [data setObject:[defaults objectForKey:@"name"] forKey:@"name"];
    [data setObject:text forKey:@"content"];
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToPOST:data route:@"comments"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self insertComment:text];
                //[self getAdmob];
                //[Common showAlert:@"Timeout. Please try again." title:@"BNA" _self:self];
                //self.loading.hidden = YES;
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSMutableArray *arr = [[payload objectForKey:@"result"] mutableCopy];
                //NSLog(@"loaded : %@",arr);
                
            });
        }
        
    }];
    [postSession resume];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
