//
//  NewsViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "NewsViewController.h"
#import "Common.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "NSDate+NVTimeAgo.h"
#import "Reachability.h"

@interface NewsViewController (){
    int page;
    NSMutableArray *newsList;
    
    UIRefreshControl *refreshControl;
    BOOL isRefreshed,isLoadMore;
    
    UIActivityIndicatorView *indicator;
    
    int adIndex,iindex;
    NSString *textSearch;
    
    CGFloat lastContentOffset;
    NSInteger curIndex;
    
    BOOL isLoading;
    
    NSUserDefaults *defaults;
    AppDelegate *appDelegate;
}

@end

@implementation NewsViewController

- (IBAction)unwindToDetail:(UIStoryboardSegue *)unwindSegue
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    adIndex = 0;
    defaults  = [NSUserDefaults standardUserDefaults];
    appDelegate = [AppDelegate appDelegate];
    newsList = [NSMutableArray new];
    
    if(self.catID != -1){
        page = 1;
        [self getNews];
        
    }else{
        self.loading.hidden = YES;
    }
    
    isLoading = NO;
    
    if(self.catID != -1){
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]!=NotReachable)
        {
            refreshControl = [[UIRefreshControl alloc]init];
            [self.tableView addSubview:refreshControl];
            [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
        }
    }
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
    [indicator setTintColor:[UIColor redColor]];
    self.tableView.tableFooterView = indicator;
    isLoadMore = NO;
    
    [self.delegate vDidLoad:self.tableView];
    
}

- (void)refreshTable {
    
    if(!isLoading){
        isLoading = YES;
        
        [indicator removeFromSuperview];
        indicator = nil;
        
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
        [indicator setTintColor:[UIColor redColor]];
        self.tableView.tableFooterView = indicator;
        
        //newsList = [NSMutableArray new];
        page = 1;
        if(self.catID != -1){
            [self getNews];
        }else{
            [self getNewsByText:textSearch];
        }
        
        isRefreshed = YES;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Function
-(void) getNews{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
            if([defaults objectForKey:[NSString stringWithFormat:@"kmcNews_%d_%d",self.catID,page]]){
                isLoading = NO;
                
                NSMutableArray *arr = [[defaults objectForKey:[NSString stringWithFormat:@"kmcNews_%d_%d",self.catID,page]] mutableCopy];
                if(arr.count > 0){
                    
                    if(arr.count < 6){
                        isLoadMore = YES;
                        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                    }
                    
                    if(isLoadMore){
                        isLoadMore = NO;
                    }
                    
                    if(isRefreshed){
                        newsList = [NSMutableArray array];
                        isRefreshed = NO;
                        [refreshControl endRefreshing];
                    }
                    
                    [newsList addObjectsFromArray:arr];
                    [self.tableView reloadData];
                    
                    self.loading.hidden = YES;
                }
            }
        }else{
            if([defaults objectForKey:[NSString stringWithFormat:@"encNews_%d_%d",self.catID,page]]){
                isLoading = NO;
                
                NSMutableArray *arr = [[defaults objectForKey:[NSString stringWithFormat:@"encNews_%d_%d",self.catID,page]] mutableCopy];
                if(arr.count > 0){
                    
                    if(arr.count < 6){
                        isLoadMore = YES;
                        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                    }
                    
                    if(isLoadMore){
                        isLoadMore = NO;
                    }
                    
                    if(isRefreshed){
                        newsList = [NSMutableArray array];
                        isRefreshed = NO;
                        [refreshControl endRefreshing];
                    }
                    
                    [newsList addObjectsFromArray:arr];
                    [self.tableView reloadData];
                    
                    self.loading.hidden = YES;
                }
            }
        }
    }else{
        NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:[NSString stringWithFormat:@"posts/%d/%d",self.catID,page]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [postSession cancel];
            
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getNews];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //NSLog(@"%@",[payload objectForKey:@"result"]);
                    
                    //Make request can load again
                    isLoading = NO;
                    
                    NSMutableArray *arr = [[payload objectForKey:@"result"] mutableCopy];
                    if(arr.count > 0){
                        
                        NSLog(@"%@",arr);
                        
                        if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
                            [defaults setObject:arr forKey:[NSString stringWithFormat:@"kmcNews_%d_%d",self.catID,page]];
                            [defaults synchronize];
                        }else{
                            [defaults setObject:arr forKey:[NSString stringWithFormat:@"encNews_%d_%d",self.catID,page]];
                            [defaults synchronize];
                        }

                        if(arr.count < 6){
                            isLoadMore = YES;
                            self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                        }
                        
                        if(isLoadMore){
                            isLoadMore = NO;
                        }
                        
                        if(isRefreshed){
                            newsList = [NSMutableArray array];
                            isRefreshed = NO;
                            [refreshControl endRefreshing];
                        }

                        AppDelegate *shareDelegate = [AppDelegate appDelegate];
                        NSMutableArray *advList = [NSMutableArray array];
                        
                        if(shareDelegate.advList.count > 0){
                            for(int i=0;i<shareDelegate.advList.count;i++){
                                if ([shareDelegate.advList[i][@"code"] isEqualToString:@"01"]){
                                    NSMutableDictionary *ad = [shareDelegate.advList[i] mutableCopy];
                                    [ad setObject:[NSNumber numberWithInt:i] forKey:@"id"];
                                    [advList addObject:ad];
                                }
                            }
                            
                            
                            NSMutableArray *temp = [arr copy];
                            adIndex = 0;
                            iindex = 0;
                            for(int i=0;i<temp.count;i++){
                                iindex = iindex +1;
                                //if((iindex) <= temp.count){
                                NSMutableDictionary *adItem_2 = [NSMutableDictionary dictionary];
                                [adItem_2 setObject:@"ad" forKey:@"type"];
                                [adItem_2 setObject:advList[adIndex] forKey:@"data"];
                                [arr insertObject:adItem_2 atIndex:iindex];
                                iindex = iindex +1;
                                
                                adIndex = adIndex + 1;
                                if(adIndex == advList.count){
                                    adIndex = 0;
                                }
                            }
                        }
                        
                        [newsList addObjectsFromArray:arr];
                        [self.tableView reloadData];
                        
                        self.loading.hidden = YES;
                    }else{
                        
                        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                        
                        isLoadMore = YES;
                        
                    }
                    
                });
            }
            
        }];
        [postSession resume];

    }
    
}
#pragma mark - TableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == newsList.count - 1) {
        if (!isLoadMore) {
            
            [indicator startAnimating];
            isLoadMore = YES;
            page = page + 1;
            if(self.catID != -1){
                [self getNews];
            }else{
                //if(newsList.count > 5){
                [self getNewsByText:textSearch];
                //}
            }
            
        }
    }
    
    NewsTableViewCell *cell;
    NSDictionary *eachNews = newsList[indexPath.row];
    
    if(![eachNews[@"type"] isEqualToString:@"ad"]){
        
        if([[defaults objectForKey:@"size"] integerValue] == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        }else if([[defaults objectForKey:@"size"] integerValue] == 1){
            cell = [tableView dequeueReusableCellWithIdentifier:@"medCell" forIndexPath:indexPath];
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"bigCell" forIndexPath:indexPath];
        }
        
        cell.lblTitle.text = eachNews[@"post_title"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        NSDate *date = [formatter dateFromString:eachNews[@"post_date"]];
        NSString *ago = [date formattedAsTimeAgo];
        cell.lblDate.text = ago;
        cell.lblViewCount.text = eachNews[@"view_count"];
        
        if(eachNews[@"article_image"][@"medium"] == [NSNull null]){
            cell.image.image = [UIImage imageNamed:@"empty150.png"];
        }else{
            [cell.image sd_setImageWithURL:[Common encodedURL:eachNews[@"article_image"][@"medium"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if(error){
                    cell.image.image = [UIImage imageNamed:@"empty150.png"];
                }
            }];
        }
        
        UIBezierPath *path;
        //cell.lblTextHeightConstraint.constant = 120;
        path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 160, 35)];
        if([[defaults objectForKey:@"size"] integerValue] == 2){
            path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 180, 50)];
        }else if([[defaults objectForKey:@"size"] integerValue] == 0){
            path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 160, 28)];
        }
        
        NSString *des = [eachNews[@"short_content"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        cell.lblTest.text = des;
        cell.lblTest.exclusionPaths = @[path];
        cell.lblTest.numberOfLines = 5;
        [cell.lblTest setFont:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];

        
        if([eachNews[@"type"]  isEqual: @"video"]){
            cell.typeImage.image = [UIImage imageNamed:@"play-button.png"];
        }else{
            cell.typeImage.image = [UIImage imageNamed:@"newspaper.png"];
        }
        
        cell.delegate = self;
        cell.indexPath = indexPath;
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"adCell" forIndexPath:indexPath];
        
        [cell.imgAd sd_setImageWithURL:[Common encodedURL:eachNews[@"data"][@"img_url"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(error){
                cell.imgAd.image = [UIImage imageNamed:@"ad_1.png"];
            }
        }];
        
    }
    
    cell.indexPath = indexPath;
    cell.delegate = self;

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return newsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *eachNews = newsList[indexPath.row];
    if([eachNews[@"type"]  isEqual: @"video"] || [eachNews[@"type"]  isEqual: @"news"]){
        
        NSString *des = [[Common stringByStrippingHTML:eachNews[@"post_content"]] stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
        des = [des stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        des = [des stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        des = [des stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        des = [des stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        
        if([des isEqualToString:@""]){
            return 135;
        }else{
            if([[defaults objectForKey:@"size"] integerValue] == 0){
                return 215;
            }else if([[defaults objectForKey:@"size"] integerValue] == 1){
                return 255;
            }else{
                return 280;
            }
        }
    }
    
    if([eachNews[@"type"] isEqualToString:@"ad"]){
        return 70;
    }
        
    if([[defaults objectForKey:@"size"] integerValue] == 0){
        return 215;
    }else if([[defaults objectForKey:@"size"] integerValue] == 1){
        return 255;
    }else{
        return 280;
    }
    
    return 235;
}

- (void) getNewsByText:(NSString *)text{
    
    textSearch = text;

    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:textSearch forKey:@"text"];
    [data setObject:[defaults objectForKey:@"lang"] forKey:@"lang"];
    [data setObject:[NSNumber numberWithInt:page] forKey:@"page"];
    
    //NSLog(@"%@",data);
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToPOST:data route:@"posts_search"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self getAdmob];
                [Common showAlert:@"Timeout. Please try again." title:@"BN Khmer" _self:self];
                self.loading.hidden = YES;
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSMutableArray *arr = [[payload objectForKey:@"result"] mutableCopy];
                
                //NSLog(@"loaded : %ld",arr.count);
                
                isLoading = NO;
                if(arr.count > 0){
                    
                    
                    //Re enable pull refresh
                    if(refreshControl == nil){
                        refreshControl = [[UIRefreshControl alloc]init];
                        [self.tableView addSubview:refreshControl];
                        [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
                    }
                    
                    //Pull to refresh
                    if(isRefreshed){
                        newsList = [NSMutableArray array];
                        isRefreshed = NO;
                        [refreshControl endRefreshing];
                    }
                    
                    //Infinite scroll
                    if(isLoadMore){
                        isLoadMore = NO;
                    }
                    
                    [newsList addObjectsFromArray:arr];
                    
                    [self.tableView reloadData];
                    self.loading.hidden = YES;
 
                }else{
                    if(newsList.count == 0){
                        [Common showAlert:@"No data" title:@"BN Khmer" _self:self];
                        newsList = [NSMutableArray array];
                        [self.tableView reloadData];
                        self.loading.hidden = YES;
                        
                        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                        isLoadMore = YES;
                        
                        [refreshControl endRefreshing];
                        [refreshControl removeFromSuperview];
                        refreshControl = nil;
                        isRefreshed = NO;
                    }else{
                        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
                        isLoadMore = YES;
                        isRefreshed = NO;

                    }
 
                }
                
            });
        }
        
    }];
    [postSession resume];
    
}

#pragma mark - Search
- (void) seachByText:(NSString *)text{
    //NSLog(@"text search : %@",text);
    
    
    self.loading.hidden = NO;
    page = 1;
    
    newsList = [NSMutableArray array];
    [self.tableView reloadData];
    
    [self getNewsByText:text];
    
}

- (void) openDetail:(NewsTableViewCell *)cell{
    
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setObject:newsList[cell.indexPath.row] forKey:@"cur"];
    //    [defaults synchronize];
    curIndex = cell.indexPath.row;
    [self performSegueWithIdentifier:@"detailSegue" sender:self];
}

- (void) openWeb{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bestnewsglobe.com/"]];
}
#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"detailSegue"]){
        UINavigationController *navController = [segue destinationViewController];
        DetailViewController *dv = (DetailViewController *)([navController viewControllers][0]);
        dv.data = [newsList[curIndex] mutableCopy];
        dv.isLocal = YES;
        dv.catID = self.catID;
    }
}


@end
