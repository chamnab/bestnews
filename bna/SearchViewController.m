//
//  SearchViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/28/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "SearchViewController.h"
#import "CarbonKit.h"
#import "Common.h"

@interface SearchViewController (){
    NewsViewController *nv;
    
    NSUserDefaults *defaults;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:@[@"search"] toolBar:self.toolbar delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.viewContainer];
    [carbonTabSwipeNavigation setNormalColor:[UIColor blackColor]];
    
    [self.searchBar becomeFirstResponder];
    
    defaults = [NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont fontWithName:[Common getRegularFont] size:15.0f]};
    
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        self.title = @"ស្វែងរក";
    }else{
        self.title = @"Search";
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [nv seachByText:self.searchBar.text];
    [self.searchBar resignFirstResponder];
}

#pragma mark - CarbonKit
- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                         viewControllerAtIndex:(NSUInteger)index {
    
    nv = [self.storyboard instantiateViewControllerWithIdentifier:@"newsView"];
    nv.delegate = self;
    
    nv.catID = -1;
    
    return nv;
    
}

- (void) hideKeyboard{
    [self.searchBar resignFirstResponder];
}

- (void) scrollingEvent:(CGFloat)deltaY{
    //NSLog(@"enter");
  
}

- (void) vDidLoad:(UITableView *)tableView{
    //tableView.hidden = YES;
    //self.shyNavBarManager.scrollView = tableView;
//    [self.shyNavBarManager setExtensionView:self.searchBar];
//    [self.shyNavBarManager setStickyExtensionView:YES];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
