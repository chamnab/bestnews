//
//  AdsTableViewCell.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/27/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "AdsTableViewCell.h"

@implementation AdsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)openWeb:(id)sender {
    [self.delegate openWeb];
}
@end
