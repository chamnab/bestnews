//
//  NewsTableViewCell.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <YYText/YYLabel.h>

@protocol NewsTableViewCell;

@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDes1;
@property (weak, nonatomic) IBOutlet UILabel *lblDes2;
@property (weak, nonatomic) IBOutlet UITextView *text;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTitleHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTextHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblViewCount;

@property (weak, nonatomic) IBOutlet UIImageView *imgAd;
@property (nonatomic) id<NewsTableViewCell> delegate;
@property (nonatomic) NSIndexPath *indexPath;
- (IBAction)openDetailEvent:(id)sender;

@property (weak, nonatomic) IBOutlet YYLabel *lblTest;

@end

@protocol NewsTableViewCell <NSObject>

-(void) openDetail:(NewsTableViewCell *)cell;

@end
