//
//  NewsTableViewCell.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "NewsTableViewCell.h"
#import "Common.h"

@implementation NewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)openDetailEvent:(id)sender {
    [self.delegate openDetail:self];
}
@end
