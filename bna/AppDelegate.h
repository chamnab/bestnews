//
//  AppDelegate.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/14/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate *) appDelegate;

@property (nonatomic) NSMutableArray *advList;
@property (nonatomic) NSMutableArray *rulesList;
@property (nonatomic) int postId;
@end

