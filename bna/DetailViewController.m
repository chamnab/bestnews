//
//  DetailViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "DetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"
#import "Common.h"
#import "YTPlayerView.h"
#import "AppDelegate.h"
#import "NSDate+NVTimeAgo.h"
#import "CommentsViewController.h"
#import <MediaPlayer/MediaPlayer.h>
//#import "CTVideoPlayerView/CTVideoViewCommonHeader.h"

@interface DetailViewController (){
    UIPageControl *pageControl;
    UIScrollView *sliderView;
    
    NSMutableArray *otherNewsList;
    NSMutableArray *advList;
    NSInteger slideIndex,totalSlideCount;
    UIButton *nextArrow,*prevArrow;
    
    NSUserDefaults *defaults;
    AppDelegate *appDelegate;
    
    CGFloat yOffset;
    
    NSMutableArray *comments;
    
    
}

@end

@implementation DetailViewController

- (IBAction)unwindToDetail:(UIStoryboardSegue *)unwindSegue
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    appDelegate = [AppDelegate appDelegate];
    
    comments = [NSMutableArray array];
    
    self.vComment.hidden = YES;
    self.vFakeComment.hidden = YES;
    self.txtComment.delegate = self;
    
    self.scrollView.delegate = self;
    self.scrollView.tag = 1;
    
    if(!self.isLocal){
        //NSLog(@"request data %d",appDelegate.postId);
        
        
        NSMutableDictionary *data = [NSMutableDictionary dictionary];
        [data setObject:[NSNumber numberWithInt:appDelegate.postId] forKey:@"post_id"];
        
        NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToPOST:data route:@"posts_detail"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [postSession cancel];
            
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[self getAdmob];
                    [Common showAlert:@"Timeout. Please try again." title:@"BN Khmer" _self:self];
                    self.loading.hidden = YES;
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                    appDelegate.postId = nil;
                    if([payload[@"status"] isEqualToString:@"success"]){
                        self.data = [payload objectForKey:@"result"];
                        [self initialize];
                    }else{
                        
                        [Common showAlert:@"Timeout. Please try again." title:@"BN Khmer" _self:self];
                    }
                    
                });
            }
            
        }];
        [postSession resume];
        
    }else{
        [self initialize];
    }

}

- (void) initialize{
    
    self.loading.hidden = YES;

    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    if(![defaults objectForKey:@"name"]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"BN Khmer" message:([[defaults objectForKey:@"lang"]  isEqual: @"km"]) ? @"សូមបញ្ចូលឈ្មេាះរបស់អ្នក" : @"Please input your nickname" preferredStyle:UIAlertControllerStyleAlert];
        if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
            [alert addAction:[UIAlertAction actionWithTitle:@"រក្សាទុក" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [defaults setObject:alert.textFields[0].text forKey:@"name"];
                [defaults synchronize];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"ចាកចេញ" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                [defaults setObject:@"ភ្ញៀវ" forKey:@"name"];
                [defaults synchronize];
            }]];
        }else{
            [alert addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [defaults setObject:alert.textFields[0].text forKey:@"name"];
                [defaults synchronize];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
                [defaults setObject:@"Guest" forKey:@"name"];
                [defaults synchronize];
            }]];
        }
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                textField.placeholder = @"បញ្ចូលឈ្មេាះរបស់អ្នក...";
            }else{
                textField.placeholder = @"Input your nickname...";
            }
            
            textField.secureTextEntry = NO;
        }];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    [self render];
}

- (void) orientationChanged:(NSNotification *)note
{
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self render];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) render{
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    sliderView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, 250)];
    sliderView.pagingEnabled = YES;
    sliderView.clipsToBounds = NO;
    sliderView.contentInset = UIEdgeInsetsZero;
    sliderView.pagingEnabled = YES;
    sliderView.backgroundColor = [UIColor whiteColor];
    sliderView.showsHorizontalScrollIndicator = NO;
    sliderView.showsVerticalScrollIndicator = NO;
    sliderView.delegate = self;
    sliderView.tag = 2;
    [self.scrollView addSubview:sliderView];
    
    NSMutableArray *images = [[self.data objectForKey:@"images"] mutableCopy];
    if(images.count > 0){
        
        if(images.count > 1){
            //Next arrow
            nextArrow = [[UIButton alloc] initWithFrame:CGRectMake(width -40 - 10, 250/2 - 20, 40, 40)];
            [nextArrow setImage:[UIImage imageNamed:@"right-arrow.png"] forState:UIControlStateNormal];
            [self.scrollView addSubview:nextArrow];
            [nextArrow addTarget:self action:@selector(nextSlide) forControlEvents:UIControlEventTouchUpInside];
            
            //Prev arrow
            prevArrow = [[UIButton alloc] initWithFrame:CGRectMake(10, 250/2 - 20, 40, 40)];
            [prevArrow setImage:[UIImage imageNamed:@"left-arrow.png"] forState:UIControlStateNormal];
            prevArrow.hidden = YES;
            [self.scrollView addSubview:prevArrow];
            [prevArrow addTarget:self action:@selector(prevSlide) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        totalSlideCount = images.count;
        slideIndex = 0;
        
        pageControl = [[UIPageControl alloc] init];
        pageControl.frame = CGRectMake(width/2,210,width,50);
        pageControl.numberOfPages = images.count;
        pageControl.currentPage = 0;
        [self.scrollView addSubview:pageControl];
        pageControl.hidden = YES;
        pageControl.backgroundColor = [UIColor clearColor];
        
        for(int i=0;i<images.count;i++){
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(width * i, 0, width, 250)];
            view.backgroundColor = [UIColor blackColor];
            view.clipsToBounds = YES;
            [sliderView addSubview:view];
            
            UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(width/2 - 20, 250/2 - 20, 40, 40)];
            loading.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [loading startAnimating];
            [view addSubview:loading];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, 250)];
            imageView.backgroundColor = [UIColor clearColor];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [imageView sd_setImageWithURL:[Common encodedURL:images[i]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if(error){
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    imageView.image = [UIImage imageNamed:@"empty1024.png"];
                }
            }];
            [view addSubview:imageView];
        }
        
        [sliderView setContentSize:CGSizeMake(width * images.count, 250)];
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 250)];
        view.backgroundColor = [UIColor clearColor];
        view.clipsToBounds = YES;
        [sliderView addSubview:view];
        
        UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(width/2 - 20, 250/2 - 20, 40, 40)];
        loading.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [loading startAnimating];
        [view addSubview:loading];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, 250)];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [imageView sd_setImageWithURL:[Common encodedURL:self.data[@"article_image"][@"large"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(error){
                imageView.image = [UIImage imageNamed:@"empty1024.png"];
            }
        }];
        [view addSubview:imageView];
    }
    
    UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake(0, sliderView.frame.size.height, width, 50)];
    actionView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:actionView];
    
    UIButton *viewCount = [UIButton buttonWithType:UIButtonTypeCustom];
    viewCount.frame = CGRectMake(5, 0, 60, CGRectGetHeight(actionView.frame));
    [viewCount setImage:[UIImage imageNamed:@"eye_big.png"] forState:UIControlStateNormal];
    //[viewCount setBackgroundColor:[UIColor lightGrayColor]];
    [viewCount setImageEdgeInsets:UIEdgeInsetsMake(35.0, -20, 35.0, 0.0)];
    [viewCount setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -40, 0.0, 0.0)];
    viewCount.imageView.contentMode = UIViewContentModeScaleAspectFit;
    viewCount.imageView.frame = CGRectMake(-20, 0, 40, 20);
    [viewCount setTitle:[NSString stringWithFormat:@"%@",self.data[@"view_count"]] forState:UIControlStateNormal];
    viewCount.titleLabel.font = [UIFont fontWithName:[Common getRegularFont] size:15.0f];
    [viewCount setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [actionView addSubview:viewCount];
    
//    UILabel *viewCount = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 45, CGRectGetHeight(actionView.frame))];
////    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
////        viewCount.text = [NSString stringWithFormat:@"%@\nអានរួច",self.data[@"view_count"]];
////    }else{
//        viewCount.text = [NSString stringWithFormat:@"%@\nview",self.data[@"view_count"]];
////    }
//    
//    viewCount.numberOfLines = 0;
//    viewCount.lineBreakMode = NSLineBreakByWordWrapping;
//    viewCount.textAlignment = NSTextAlignmentCenter;
//    [viewCount setFont:[UIFont fontWithName:[Common getRegularFont] size:15.0f]];
//    [actionView addSubview:viewCount];
    
    UIButton *commentCount = [UIButton buttonWithType:UIButtonTypeCustom];
    commentCount.frame = CGRectMake(viewCount.frame.origin.x + CGRectGetWidth(viewCount.frame) + 5, 0, 65, CGRectGetHeight(actionView.frame));
    [commentCount setImage:[UIImage imageNamed:@"chat.png"] forState:UIControlStateNormal];
    //[commentCount setBackgroundColor:[UIColor lightGrayColor]];
    [commentCount setImageEdgeInsets:UIEdgeInsetsMake(35, -20, 35, 0.0)];
    [commentCount setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -40, 0.0, 0.0)];
    commentCount.imageView.contentMode = UIViewContentModeScaleAspectFit;
    commentCount.imageView.frame = CGRectMake(-20, 0, 40, 20);
    [commentCount setTitle:[NSString stringWithFormat:@"%@",self.data[@"comment_count"]] forState:UIControlStateNormal];
    commentCount.titleLabel.font = [UIFont fontWithName:[Common getRegularFont] size:15.0f];
    [commentCount setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [actionView addSubview:commentCount];
    
//    UILabel *commentCount = [[UILabel alloc] initWithFrame:CGRectMake(viewCount.frame.origin.x + CGRectGetWidth(viewCount.frame) + 5, 0, 65, CGRectGetHeight(actionView.frame))];
//        commentCount.text = [NSString stringWithFormat:@"%@\ncomment",self.data[@"comment_count"]];
//
//    
//    commentCount.numberOfLines = 0;
//    commentCount.lineBreakMode = NSLineBreakByWordWrapping;
//    commentCount.textAlignment = NSTextAlignmentCenter;
//    [commentCount setFont:[UIFont fontWithName:[Common getRegularFont] size:15.0f]];
//    [actionView addSubview:commentCount];
    
    //UIButton *btnShare = [[UIButton alloc] initWithFrame:CGRectMake(commentCount.frame.origin.x + CGRectGetWidth(commentCount.frame) + 10, CGRectGetHeight(actionView.frame)/2 - 15, 30, 30)];
    UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    btnShare.frame = CGRectMake(commentCount.frame.origin.x + commentCount.frame.size.width, CGRectGetHeight(actionView.frame)/2 - 20, 100, 40);
    [btnShare setImage:[UIImage imageNamed:@"share_option.png"] forState:UIControlStateNormal];
    //[btnShare setBackgroundColor:[UIColor lightGrayColor]];
    [btnShare setImageEdgeInsets:UIEdgeInsetsMake(30.0, -20.0, 30.0, 0.0)];
    [btnShare setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -35.0, 0.0, 0.0)];
    btnShare.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btnShare.imageView.frame = CGRectMake(-20, 0, 40, 20);
    [btnShare setTitle:@"share" forState:UIControlStateNormal];
    btnShare.titleLabel.font = [UIFont fontWithName:[Common getRegularFont] size:15.0f];
    [btnShare setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [actionView addSubview:btnShare];
    [btnShare addTarget:self action:@selector(shareEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect titleRect = [Common getTextHeight:self.data[@"post_title"] font:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]] width:width - 10];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5,actionView.frame.origin.y + actionView.frame.size.height - 5, width - 10, titleRect.size.height)];
    [lblTitle setFont:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]]];
    lblTitle.textColor = [UIColor colorWithRed:0/255.0f green:114/255.0f blue:188/255.0f alpha:1.0];
    lblTitle.text = self.data[@"post_title"];
    lblTitle.numberOfLines = 0;
    [self.scrollView addSubview:lblTitle];
    
    UIImageView *timeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, lblTitle.frame.origin.y + CGRectGetHeight(lblTitle.frame)+2, 18, 18)];
    timeIcon.image = [UIImage imageNamed:@"schedule.png"];
    [self.scrollView addSubview:timeIcon];
    
    UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(25,lblTitle.frame.origin.y + CGRectGetHeight(lblTitle.frame)+ 1 , width, 20)];
    lblTime.font = [UIFont fontWithName:[Common getRegularFont] size:12.0f];
    lblTime.textColor = [UIColor lightGrayColor];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
    NSDate *date = [formatter dateFromString:self.data[@"post_date"]];
    NSString *ago = [date formattedAsTimeAgo];
    lblTime.text = ago;
    
    [self.scrollView addSubview:lblTime];
    
    CGRect desRect = [Common getTextHeight:self.data[@"post_content"] font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]] width:width - 10];
    
    UITextView *tvDes = [[UITextView alloc] initWithFrame:CGRectMake(5, lblTitle.frame.origin.y + CGRectGetHeight(lblTitle.frame)+20, width - 10, desRect.size.height)];

    tvDes.text = self.data[@"post_content"];
    tvDes.scrollEnabled = NO;
    tvDes.editable = NO;
    tvDes.selectable = NO;
    tvDes.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
    [self.scrollView addSubview:tvDes];
    
    NSMutableArray *videoList = [self.data[@"video"] mutableCopy];
    
    yOffset = tvDes.frame.origin.y + CGRectGetHeight(tvDes.frame) + 5;
    for (int i=0;i<videoList.count;i++){
        NSMutableDictionary *item = [videoList[i] mutableCopy];
        if([item[@"type"] isEqualToString:@"youtube"]){
            YTPlayerView *videoView = [[YTPlayerView alloc] initWithFrame:CGRectMake(5, yOffset, width - 10, 250)];
            videoView.backgroundColor = [UIColor blackColor];
            [videoView loadWithVideoId:item[@"id"]];
            [self.scrollView addSubview:videoView];
        }else if([item[@"type"] isEqualToString:@"video"]){
                NSLog(@"url : %@",self.data[@"article_image"][@"large"]);
//            CTVideoView *ctView = [[CTVideoView alloc] initWithFrame:CGRectMake(5, yOffset, width - 10, 250)];
//            [self.scrollView addSubview:ctView];
//            ctView.videoUrl = [NSURL URLWithString:item[@"id"]];
//            [ctView play];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, yOffset, width - 10, 250)];
            view.layer.borderWidth = 1.0f;
            view.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [self.scrollView addSubview:view];
            
            UIButton *videoThumnail = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 250)];
            [videoThumnail sd_setImageWithURL:[Common encodedURL:self.data[@"article_image"][@"large"]] forState:UIControlStateNormal];
            videoThumnail.imageView.contentMode = UIViewContentModeScaleAspectFill;
            videoThumnail.tag = i;
            [view addSubview:videoThumnail];
            [videoThumnail addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *imgPlay = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width/2 - 30, view.frame.size.height/2 -30, 60, 60)];
            imgPlay.image =[UIImage imageNamed:@"play-button-big.png"];
            [view addSubview:imgPlay];
            
        }else if([item[@"type"] isEqualToString:@"html"]){
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, yOffset, width - 10, 250)];
            view.layer.borderWidth = 1.0f;
            view.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [self.scrollView addSubview:view];
            
            UIActivityIndicatorView *vLoading = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((width-10)/2-20,250/2-20 , 40, 40)];
            vLoading.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [vLoading startAnimating];
            [view addSubview:vLoading];
            
            NSString *embedHTML = [NSString stringWithFormat:@"<iframe width=\"%@\" height=\"%d\" src=\"%@\" frameborder=\"0\" allowfullscreen></iframe>",@"100%",260,@"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fgillionnews%2Fvideos%2F1082965888408241%2F&amp;"];
            ;
            NSString *html = [NSString stringWithFormat:@"%@", embedHTML];

            UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame))];
            webview.scrollView.scrollEnabled = NO;
            [webview loadHTMLString:html baseURL:nil];
            //[webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:html]]];
            [view addSubview:webview];
            
        }
        
        yOffset = yOffset+ 255;
    }
    
//    if(videoList.count > 0){
//        yOffset = yOffset - 255;
//    }
    
    AppDelegate *shareDelegate = [AppDelegate appDelegate];
    advList = [NSMutableArray array];
    for(int i=0;i<shareDelegate.advList.count;i++){
        if ([shareDelegate.advList[i][@"code"] isEqualToString:@"03"]){
            NSMutableDictionary *ad = [shareDelegate.advList[i] mutableCopy];
            [ad setObject:[NSNumber numberWithInt:i] forKey:@"id"];
            [advList addObject:ad];
        }
    }
    
    for (int i=0;i<advList.count;i++){
        UIButton *imageView = [[UIButton alloc] initWithFrame:CGRectMake(0, yOffset, width, 48)];
        imageView.tag = i;
        [imageView sd_setImageWithURL:[Common encodedURL:advList[i][@"img_url"]] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(error){
                [imageView setImage:[UIImage imageNamed:@"ad_1.png"] forState:UIControlStateNormal];
            }
        }];
        
        [imageView addTarget:self action:@selector(openWeb:) forControlEvents:UIControlEventTouchUpInside];

        [self.scrollView addSubview:imageView];
    
        yOffset =yOffset + 48+5;
    }
    
//    UIButton *btnGotoComment = [[UIButton alloc] initWithFrame:CGRectMake(5, yOffset + 5, width -10, 40)];
//    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
//        [btnGotoComment setTitle:@"បញ្ចេញមតិ" forState:UIControlStateNormal];
//    }else{
//        [btnGotoComment setTitle:@"Comments" forState:UIControlStateNormal];
//    }
//    [btnGotoComment setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//    btnGotoComment.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    btnGotoComment.layer.borderWidth = 2.0f;
//    btnGotoComment.layer.cornerRadius = 5.0f;
//    btnGotoComment.titleLabel.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
//    [self.scrollView addSubview:btnGotoComment];
//    [btnGotoComment addTarget:self action:@selector(openCommentEvent) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView setContentSize:CGSizeMake(width, yOffset)];
    if(comments.count != 0){
        [self renderCommentBlock];
    }else{
       [self getComments:yOffset];
    }
    

}

-(void)openWeb:(UIButton *)sender{
    NSLog(@"%ld",(long)sender.tag);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:advList[sender.tag][@"url"]]];
    
}

- (void)openCommentEvent{
    [self performSegueWithIdentifier:@"commentSegue" sender:self];
}

- (void) getComments:(CGFloat)offset{
    //NSLog(@"%d",self.catID);
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:[NSString stringWithFormat:@"comment/%d",[self.data[@"ID"] intValue]]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self getAdmob];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                //NSLog(@"%@",payload[@"result"]);
                
                self.vComment.hidden = NO;
                comments = payload[@"result"];
                [self renderCommentBlock];
                
            });
        }
        
    }];
    [postSession resume];
    
}

- (void) renderCommentBlock{
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    int heightBlock = 0;
    for (int i=0;i<comments.count;i++){
        
        NSString *author = comments[i][@"comment_author"];
        NSString *content = comments[i][@"comment_content"];
        
        CGRect titleRect = [Common getTextHeight:author font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]] width:screenWidth - 10];
        
        CGRect contentRect = [Common getTextHeight:content font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]] width:screenWidth - 10];
        
        heightBlock = titleRect.size.height+ contentRect.size.height + 20;
        
        UIView *vOther = [[UIView alloc] initWithFrame:CGRectMake(0, yOffset + heightBlock * i, screenWidth,heightBlock )];
        [self.scrollView addSubview:vOther];
        
        UILabel *lblAuthor = [[UILabel alloc] initWithFrame:CGRectMake(10, 2.5, screenWidth - 25,titleRect.size.height)];
        lblAuthor.text = comments[i][@"comment_author"];
        lblAuthor.textColor = [Common getColor];
        lblAuthor.lineBreakMode = NSLineBreakByCharWrapping;
        lblAuthor.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]];
        [vOther addSubview:lblAuthor];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        NSDate *date = [formatter dateFromString:comments[i][@"comment_date"]];
        NSString *ago = [date formattedAsTimeAgo];
        
        UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(10, lblAuthor.frame.origin.y + titleRect.size.height, screenWidth - 25,15)];
        lblDate.text = ago;
        lblDate.textColor = [UIColor lightGrayColor];
        lblDate.lineBreakMode = NSLineBreakByCharWrapping;
        lblDate.font = [UIFont fontWithName:[Common getRegularFont] size:10.0f];
        [vOther addSubview:lblDate];
        
        UILabel *lblContent = [[UILabel alloc] initWithFrame:CGRectMake(10, lblDate.frame.origin.y + lblDate.frame.size.height, screenWidth - 25,contentRect.size.height)];
        lblContent.text = comments[i][@"comment_content"];
        lblContent.textColor = [UIColor blackColor];
        lblContent.lineBreakMode = NSLineBreakByCharWrapping;
        lblContent.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
        [vOther addSubview:lblContent];
        
        UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(vOther.frame), screenWidth - 25, 1)];
        divider.backgroundColor = [UIColor grayColor];
        [vOther addSubview:divider];
        
    }
    
    yOffset = 50 + yOffset + heightBlock * comments.count;
    [self.scrollView setContentSize:CGSizeMake(screenWidth,yOffset)];
}

- (void) getOtherNews:(CGFloat)offset{
    //NSLog(@"%d",self.catID);
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:[NSString stringWithFormat:@"posts_related/%d",self.catID]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self getAdmob];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                //NSLog(@"%@",payload[@"result"]);
                
                CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
                otherNewsList = payload[@"result"];
                
                int heightBlock = 60;
                if([[defaults objectForKey:@"size"] integerValue] == 2){
                    heightBlock = 75;
                }
                
                for (int i=0;i<otherNewsList.count;i++){
 
                    UIView *vOther = [[UIView alloc] initWithFrame:CGRectMake(0, offset + heightBlock * i, screenWidth, heightBlock)];
                    [self.scrollView addSubview:vOther];
                    
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 2.5, screenWidth - 25,CGRectGetHeight(vOther.frame) - 5)];
                    label.text = otherNewsList[i][@"comment_content"];
                    label.textColor = [UIColor blackColor];
                    label.lineBreakMode = NSLineBreakByCharWrapping;
                    label.textAlignment = NSTextAlignmentLeft;
                    label.numberOfLines = 2;
                    label.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
                    [vOther addSubview:label];
                    
                    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 2.5, screenWidth - 25,CGRectGetHeight(vOther.frame) - 5)];
                    button.tag = i;
                    [vOther addSubview:button];
                    
                    [button addTarget:self action:@selector(otherTouchDown:) forControlEvents:UIControlEventTouchUpInside];
                    
                    //if (i != otherNewsList.count-1){
                        UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(vOther.frame), screenWidth - 25, 1)];
                        divider.backgroundColor = [UIColor grayColor];
                        [vOther addSubview:divider];
                    //}
                    //lastestOffset = lastestOffset + 60;
                    
                }
                
                [self.scrollView setContentSize:CGSizeMake(screenWidth, offset + heightBlock * otherNewsList.count)];
                
            });
        }
        
    }];
    [postSession resume];
    
}

- (void) otherTouchDown:(UIButton *)sender{
    
    self.data = otherNewsList[sender.tag];
    self.scrollView.hidden = YES;
    self.loading.hidden = NO;
    [self.scrollView setContentOffset:CGPointZero animated:NO];
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self render];
    
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(clearLoading)
                                   userInfo:nil
                                    repeats:NO];

    
}

- (void)clearLoading{
    self.scrollView.hidden = NO;
    self.loading.hidden = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
//    if(scrollView.tag == 1){
//            }
    
    if(scrollView.tag == 2){
        CGFloat pageWidth = sliderView.frame.size.width;
        float fractionalPage = sliderView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page;
        slideIndex = page;
        
        if(slideIndex == 0){
            prevArrow.hidden = YES;
            nextArrow.hidden = NO;
        }else if(slideIndex == totalSlideCount - 1){
            prevArrow.hidden = NO;
            nextArrow.hidden = YES;

        }else if(slideIndex < totalSlideCount - 1) {
            prevArrow.hidden = NO;
            nextArrow.hidden = NO;
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier  isEqual: @"commentSegue"]){
        UINavigationController *ng = segue.destinationViewController;
        CommentsViewController *cv = [ng.viewControllers objectAtIndex:0];
        cv.post_id = [self.data[@"ID"] intValue];
    }
}

- (IBAction)shareEvent:(id)sender {
    
    // create a message
    NSString *theMessage = self.data[@"guid"];
    NSArray *items = @[theMessage];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentActivityController:controller];
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
    
}

- (void)keyboardDidShow: (NSNotification *) notification{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    if([defaults objectForKey:@"name"]){
        self.bottomHeightConstraint.constant = CGRectGetHeight(keyboardFrame) - self.vAd.frame.size.height;
        self.vFakeComment.hidden = NO;
    }
}

- (void)keyboardDidHide: (NSNotification *) notif{
    
    if([defaults objectForKey:@"name"]){
        self.bottomHeightConstraint.constant = 0;
        self.vFakeComment.hidden = YES;
    }
    
}

- (void) nextSlide{
    slideIndex = slideIndex + 1;
    [sliderView setContentOffset:CGPointMake(sliderView.frame.size.width*slideIndex, 0.0f) animated:YES];
}

- (void) prevSlide{
    slideIndex = slideIndex - 1;
    [sliderView setContentOffset:CGPointMake(sliderView.frame.size.width*slideIndex, 0.0f) animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtComment resignFirstResponder];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    NSString *author = [defaults objectForKey:@"name"];
    NSString *content = self.txtComment.text;
    
    CGRect titleRect = [Common getTextHeight:author font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]] width:screenWidth - 10];
    
    CGRect contentRect = [Common getTextHeight:content font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]] width:screenWidth - 10];
    
    CGFloat heightBlock = titleRect.size.height+ contentRect.size.height + 20;
    
    UIView *vOther = [[UIView alloc] initWithFrame:CGRectMake(0, yOffset - 50, screenWidth,heightBlock )];
    [self.scrollView addSubview:vOther];
    
    UILabel *lblAuthor = [[UILabel alloc] initWithFrame:CGRectMake(10, 2.5, screenWidth - 25,titleRect.size.height)];
    lblAuthor.text = author;
    lblAuthor.textColor = [Common getColor];
    lblAuthor.lineBreakMode = NSLineBreakByCharWrapping;
    lblAuthor.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"title"] floatValue]];
    [vOther addSubview:lblAuthor];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
    
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    
    NSDate *date = [formatter dateFromString:stringFromDate];
    NSString *ago = [date formattedAsTimeAgo];
    
    UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(10, lblAuthor.frame.origin.y + titleRect.size.height, screenWidth - 25,15)];
    lblDate.text = ago;
    lblDate.textColor = [UIColor lightGrayColor];
    lblDate.lineBreakMode = NSLineBreakByCharWrapping;
    lblDate.font = [UIFont fontWithName:[Common getRegularFont] size:10.0f];
    [vOther addSubview:lblDate];
    
    UILabel *lblContent = [[UILabel alloc] initWithFrame:CGRectMake(10, lblDate.frame.origin.y + lblDate.frame.size.height, screenWidth - 25,contentRect.size.height)];
    lblContent.text = content;
    lblContent.textColor = [UIColor blackColor];
    lblContent.lineBreakMode = NSLineBreakByCharWrapping;
    lblContent.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
    [vOther addSubview:lblContent];
    
    UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(vOther.frame), screenWidth - 25, 1)];
    divider.backgroundColor = [UIColor grayColor];
    [vOther addSubview:divider];
    
    [self insertComment:self.txtComment.text];
    self.txtComment.text = @"";
    
    yOffset = yOffset + heightBlock;
    
    [self.scrollView setContentSize:CGSizeMake(screenWidth, yOffset)];
    
    return NO;
}

- (void) insertComment:(NSString *)text{
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:[NSNumber numberWithInt:[self.data[@"ID"] intValue]] forKey:@"post_id"];
    [data setObject:[defaults objectForKey:@"name"] forKey:@"name"];
    [data setObject:text forKey:@"content"];
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToPOST:data route:@"comments"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self insertComment:text];
                //[self getAdmob];
                //[Common showAlert:@"Timeout. Please try again." title:@"BNA" _self:self];
                //self.loading.hidden = YES;
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                //NSMutableArray *arr = [[payload objectForKey:@"result"] mutableCopy];
                if(![payload[@"status"] isEqualToString:@"success"]){
                    [self insertComment:text];
                }
                //NSLog(@"loaded : %@",arr);
                
            });
        }
        
    }];
    [postSession resume];
    
}

- (void) playVideo:(UIButton *)sender{
    NSMutableArray *videoList = [self.data[@"video"] mutableCopy];
    for (int i=0;i<videoList.count;i++){
        if (sender.tag == i){
                NSURL *movieURL = [NSURL URLWithString:videoList[i][@"id"]];
                MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
            
            
                //[self presentViewController:movieController animated:YES completion:nil];
                [self presentMoviePlayerViewControllerAnimated:movieController];
                [movieController.moviePlayer prepareToPlay];
                [movieController.moviePlayer play];
            return;
        }
    }
}

- (IBAction)closeCommentEvent:(id)sender {
    self.bottomHeightConstraint.constant = 0;
    [self.txtComment resignFirstResponder];
    self.vFakeComment.hidden = YES;

}
@end
