//
//  ViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/14/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarbonKit.h"

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CarbonTabSwipeNavigationDelegate>

@property (strong, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIView *navView;
@property (weak, nonatomic) IBOutlet UIImageView *topBarAd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMenuBarConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHotNewsBarConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBarConstraint;
@property (weak, nonatomic) IBOutlet UIView *hotNewsView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *maqueeContainer;
@property (weak, nonatomic) IBOutlet UILabel *hotNewsMaquee;
@property (weak, nonatomic) IBOutlet UIView *loading;
@property (weak, nonatomic) IBOutlet UITableView *tbMenuList;
- (IBAction)menuClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *menuContainer;
- (IBAction)clearMenuClick:(id)sender;
- (IBAction)searchAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLang;

- (IBAction)switchingEvent:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHotNewsTItle;

@end

