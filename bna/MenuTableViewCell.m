//
//  MenuTableViewCell.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/27/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
