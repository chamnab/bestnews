//
//  SettingViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 11/3/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLRadioButton.h"

@interface SettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet DLRadioButton *btnFont;
@property (weak, nonatomic) IBOutlet UIView *vFontGroup;
@property (weak, nonatomic) IBOutlet UIView *vNotifGroup;
@property (weak, nonatomic) IBOutlet DLRadioButton *btnSmallFont;
@property (weak, nonatomic) IBOutlet DLRadioButton *btnOnNoti;

- (IBAction)fontSizeEvent:(DLRadioButton *)sender;
- (IBAction)notiEvent:(DLRadioButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblFontDes;
@property (weak, nonatomic) IBOutlet UILabel *lblNotiDes;

@end
