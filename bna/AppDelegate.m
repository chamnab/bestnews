//
//  AppDelegate.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/14/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "AppDelegate.h"
#import "OneSignal.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate *) appDelegate{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"lang"] == nil){
        [defaults setObject:@"km" forKey:@"lang"];
        [defaults setObject:@1 forKey:@"size"];
        [defaults setObject:@0 forKey:@"noti"];
        [defaults setObject:@0 forKey:@"fontReset"];
        [OneSignal sendTag:@"lang" value:@"km"];
        [defaults synchronize];
    }
    
    NSMutableArray *rules = [NSMutableArray array];
    //Define rule
    NSMutableDictionary *smallRule = [NSMutableDictionary dictionary];
    [smallRule setValue:@17 forKey:@"title"];
    [smallRule setValue:@15 forKey:@"detail"];
    [rules addObject:smallRule];
    
    NSMutableDictionary *medRule = [NSMutableDictionary dictionary];
    [medRule setValue:@19 forKey:@"title"];
    [medRule setValue:@17 forKey:@"detail"];
    [rules addObject:medRule];
    
    NSMutableDictionary *bigRule = [NSMutableDictionary dictionary];
    [bigRule setValue:@21 forKey:@"title"];
    [bigRule setValue:@19 forKey:@"detail"];
    [rules addObject:bigRule];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    self.rulesList = rules;
    
    [OneSignal initWithLaunchOptions:launchOptions appId:@"451816ef-e0c1-4579-8612-d58f70b81f1c" handleNotificationAction:^(OSNotificationOpenedResult *result) {
        
        // This block gets called when the user reacts to a notification received
        OSNotificationPayload* payload = result.notification.payload;
        
        if (payload.additionalData) {
            NSLog(@"%@",payload.additionalData);
            self.postId = [payload.additionalData[@"post_id"] intValue];
        }
    }];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
