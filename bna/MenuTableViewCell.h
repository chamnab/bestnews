//
//  MenuTableViewCell.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/27/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
