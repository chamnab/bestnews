//
//  Common.m
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/22/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "Common.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6_OR_MORE (IS_IPHONE && SCREEN_MAX_LENGTH > 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


@implementation Common
static NSMutableArray *tagsList;

+ (BOOL) isIphone4 {
    return IS_IPHONE_4_OR_LESS;
}
+ (BOOL) isIphone6 {
    return IS_IPHONE_6;
}
+ (BOOL) isIphone6P{
    return IS_IPHONE_6P;
}
+ (BOOL) isBiggerThaniPhone5{
    return  IS_IPHONE_6_OR_MORE;
}

+ (NSString *) getToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults valueForKey:@"access_token"];
    return token;
}

+ (NSString *) getLang{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lang = [defaults valueForKey:@"lang"];
    NSString *_la;
    if([lang  isEqual: @"km_KH"]){
        _la = @"km";
    } else if([lang  isEqual: @"en"]){
        _la = @"en";
    }else if([lang  isEqual: @"ja_JP"]){
        _la = @"jp";
    }
    
    return _la;
}

+ (void) showAlert:(NSString *)msg title:(NSString *)title _self:(UIViewController *)_self{
    
    if (title == nil){
        title = @"BN Khmer";
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [_self presentViewController:alertController animated:YES completion:nil];
}

+ (UIAlertController *) getRetryAlert{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Watami" message:[Common getLocaleText:@"retry_message"] preferredStyle:UIAlertControllerStyleAlert];
    
    return alertController;
    
}

+ (NSURL *) getBaseURL:(NSString *)route{
    
    if([route isEqualToString:@"advertise"]){
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/wp-json/api/1/%@?e=%@",route,[self randomStringWithLength:5]]]);
        return [NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/wp-json/api/1/%@?e=%@",route,[self randomStringWithLength:5]]];
    }
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/wp-json/api/1/%@?e=%@",route,[self randomStringWithLength:5]]]);
        return [NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/wp-json/api/1/%@?e=%@",route,[self randomStringWithLength:5]]];
    }else{
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/en/wp-json/api/1/%@?e=%@",route,[self randomStringWithLength:5]]]);
        return [NSURL URLWithString:[NSString stringWithFormat:@"http://bestnewsglobe.com/en/wp-json/api/1/%@",route]];
    }
    
}

+ (NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}


+ (NSMutableURLRequest *)convertToPOST:(NSMutableDictionary*)data route:(NSString *)route{
    
    NSError *error;
    NSData *postJSONData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self getBaseURL:route]];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = postJSONData;
    
    return request;
}

+ (NSMutableURLRequest *)convertToGET:(NSString *)route{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self getBaseURL:route]];
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    return request;
}

+ (void) changeNavigationColor:(UIViewController *)cSelf{
    cSelf.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:35/255.0f green:31/255.0f blue:32/255.0f alpha:1.0f];
    [cSelf.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

+ (NSString *) getLocaleText:(NSString*)code{
    NSString *text = @"NULL";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *langList = [defaults objectForKey:[defaults objectForKey:@"lang"]];
    //NSLog(@"%@",langList);
    text = [langList objectForKey:code];
    return text;
}

+ (void) trackScreen:(NSString *) name{
    /*id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];*/
}

+ (void) trackEvent:(NSString *) action{
    /*id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Detail"
                                                          action:action
                                                           label:nil
                                                           value:nil] build]];*/
}

+ (void) changeFont:(NSArray *)labelList fontSize:(CGFloat)fontSize{
    for (int i=0;i<labelList.count;i++){
        UILabel *label = labelList[i];
        CGFloat sizeFont = fontSize;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *lang = [defaults valueForKey:@"lang"];
        if([lang isEqualToString:@"km_KH"]){
            sizeFont = sizeFont - 2;
            label.font = [UIFont fontWithName:@"Khmer OS Content" size:sizeFont];
        }
        if([lang isEqualToString:@"ja_JP"]){
            sizeFont  = sizeFont + 1;
            label.font = [UIFont fontWithName:@"Honoka Mincho" size:sizeFont];
        }
        if([lang isEqualToString:@"en"]){
            label.font = [UIFont fontWithName:@"Helvetica Neue" size:sizeFont];
        }
    }
}

+ (void) changeNavigationFont:(UIViewController *) _self{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lang = [defaults valueForKey:@"lang"];
    if([lang isEqualToString:@"km_KH"]){
        [_self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Khmer OS Content" size:16]}];
    }
    if([lang isEqualToString:@"ja_JP"]){
        [_self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Honoka Mincho" size:18]}];
    }
    if([lang isEqualToString:@"en"]){
        [_self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:18]}];
    }
}

+ (NSMutableArray *) getTagCastList{
    return tagsList;
}

+ (CGRect) getTextHeight:(NSString *)text font:(UIFont *)font width:(CGFloat) width{
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:font}];
    CGRect paragraphRect = [attributedText boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                        context:nil];
    
    return paragraphRect;
}
+ (UIColor *) getColor{
    return [UIColor colorWithRed:40/255.0f green:86/255.0f blue:164/255.0f alpha:1.0];
}

+ (NSString *) getRegularFont{
    return @"Battambang";
}
+ (NSString *) getBoldFont{
    return @"Battambang-Bold";
}

+ (NSString *) stringByStrippingHTML:(NSString *)s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

+ (NSURL *) encodedURL:(NSString *)url{
    return [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

+ (NSString *) convertDate:(NSString *)date{
    NSArray *array = [date componentsSeparatedByString:@" "];
    
    NSMutableDictionary *month = [NSMutableDictionary dictionary];
    month[@"january"] = @"មករា";
    month[@"februrary"] = @"កុម្ភ";
    month[@"march"] = @"មីនា";
    month[@"april"] = @"មេសា";
    month[@"may"] = @"ឧសភា";
    month[@"june"] = @"មិថុនា";
    month[@"july"] = @"កក្កដា";
    month[@"august"] = @"សីហា";
    month[@"september"] = @"កញ្ញា";
    month[@"october"] = @"តុលា";
    month[@"november"] = @"វិចិ្ឆកា";
    month[@"december"] = @"ធ្នូ";
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        NSString *day = [array[1] stringByReplacingOccurrencesOfString:@"st" withString:@""];
        day = [day stringByReplacingOccurrencesOfString:@"th" withString:@""];
        day = [day stringByReplacingOccurrencesOfString:@"nd" withString:@""];
        day = [day stringByReplacingOccurrencesOfString:@"rd" withString:@""];
        return [NSString stringWithFormat:@"%@ %@ %@",month[[array[0] lowercaseString]],day,array[2]];
    }else{
        return date;
    }
}

+ (NSMutableDictionary *) getMonthList{
    NSMutableDictionary *month = [NSMutableDictionary dictionary];
    month[@"january"] = @"មករា";
    month[@"februrary"] = @"កុម្ភ";
    month[@"march"] = @"មីនា";
    month[@"april"] = @"មេសា";
    month[@"may"] = @"ឧសភា";
    month[@"june"] = @"មិថុនា";
    month[@"july"] = @"កក្កដា";
    month[@"august"] = @"សីហា";
    month[@"september"] = @"កញ្ញា";
    month[@"october"] = @"តុលា";
    month[@"november"] = @"វិចិ្ឆកា";
    month[@"december"] = @"ធ្នូ";
    
    return month;
}

+ (NSMutableDictionary *) getDayList{
    NSMutableDictionary *date = [NSMutableDictionary dictionary];
    date[@"monday"] = @"ច័ន្ទ";
    date[@"tuesday"] = @"អង្គារ";
    date[@"wednesday"] = @"ពុធ";
    date[@"thursday"] = @"ព្រហ";
    date[@"friday"] = @"សុក្រ";
    date[@"saturday"] = @"សៅរ៍";
    date[@"sunday"] = @"អាទិត្យ";
    return date;
}
@end
