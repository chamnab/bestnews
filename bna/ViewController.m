//
//  ViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/14/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "ViewController.h"
#import "Common.h"
#import "NewsViewController.h"
#import "DetailViewController.h"
#import "MarqueeLabel.h"
#import "MenuTableViewCell.h"
#import "AppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"
#import "OneSignal.h"
#import "Reachability.h"

@interface ViewController (){
    NSMutableArray *categories;
    NSMutableArray *drawer_categories;
    NSMutableArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSUserDefaults *defaults;
    AppDelegate *appDelegate;
    MarqueeLabel *hotNewsMaquee;
    
    BOOL isChangedLang;
    
}

@end

@implementation ViewController

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.hotNewsView.layer.borderWidth = 1.0;
    self.hotNewsView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.menuContainer.hidden = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont fontWithName:[Common getRegularFont] size:15.0f]};
    
    
    defaults= [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        [self.btnLang setImage:[UIImage imageNamed:@"gb.png"] forState:UIControlStateNormal];
        self.lblHotNewsTItle.text = @"  ថ្មីបំផុត";
        //self.title = @"ទំព័រដើម";
    }else{
        [self.btnLang setImage:[UIImage imageNamed:@"kh.png"] forState:UIControlStateNormal];
        self.lblHotNewsTItle.text = @"  Latest";
        //self.title = @"Home";
    }
    
    if([[defaults objectForKey:@"fontReset"] intValue]== 1){
        [defaults setObject:@0 forKey:@"fontReset"];
        
        self.loading.hidden = NO;
        
        [drawer_categories removeAllObjects];
        drawer_categories = [NSMutableArray array];
        [self.tbMenuList reloadData];
        
        [categories removeAllObjects];
        categories = [NSMutableArray array];
        
        
        [[self.viewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [[self.maqueeContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [[self.toolbar subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            [self getDrawerMenu];
            [self getMarqueeText];
            [self getCategory];
        }
        else{
            [self getDrawerMenu];
            [self getMarqueeText];
            [self getAds];
        }
        
        
    }
    
    if(hotNewsMaquee){
        hotNewsMaquee.frame = CGRectMake(0, self.heightHotNewsBarConstraint.constant/2 - 50/2, CGRectGetWidth(self.maqueeContainer.frame), 50);
        //        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        //        if(UIInterfaceOrientationIsPortrait(interfaceOrientation)){
        //            NSLog(@"portrait");
        //        }
        //        if(UIInterfaceOrientationIsLandscape(interfaceOrientation)){
        //            NSLog(@"land");
        //        }
        //NSLog(@"----qw %@",deviceOrientation);
    }
    
    if(appDelegate.postId){
        [self performSegueWithIdentifier:@"detailSegue" sender:self];
    }
    
}

- (IBAction)unwindToHome:(UIStoryboardSegue *)unwindSegue
{
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = [AppDelegate appDelegate];
    defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self getDrawerMenu];
        [self getMarqueeText];
        [self getCategory];
    }else{
        [self getDrawerMenu];
        [self getMarqueeText];
        [self getAds];
    }
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    
    self.baseView.backgroundColor = [UIColor colorWithRed:0/255.0 green:114/255.0 blue:188/255.0 alpha:1.0];
    self.navView.backgroundColor = [UIColor colorWithRed:0/255.0 green:114/255.0 blue:188/255.0 alpha:1.0];
    
    //self.heightBarConstraint.constant = self.navigationController.navigationBar.frame.size.height - 15;
    //self.topMenuBarConstraint.constant = self.navigationController.navigationBar.frame.size.height - 15;
    [self.navigationController setNavigationBarHidden:YES];
    
    
    
    isChangedLang = NO;
    
    
}

- (void) getAds{
    
    NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:@"advertise"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [postSession cancel];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getAds];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                AppDelegate *sharedDelegate = [AppDelegate appDelegate];
                sharedDelegate.advList = payload[@"result"];
                
                //Advetise topbar
                AppDelegate *shareDelegate = [AppDelegate appDelegate];
                for(int i=0;i<shareDelegate.advList.count;i++){
                    if ([shareDelegate.advList[i][@"code"] isEqualToString:@"04"]){
                        [self.topBarAd sd_setImageWithURL:[Common encodedURL:shareDelegate.advList[i][@"img_url"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            if(error){
                                //imageView.image = [UIImage imageNamed:@"empty1024.png"];
                            }
                        }];
                        break;
                    }
                }
                
                [self getCategory];
                
            });
        }
        
    }];
    [postSession resume];
    
}

- (void) getMarqueeText{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
            if([defaults objectForKey:@"kmcMarquee"]){
                
                hotNewsMaquee = [[MarqueeLabel alloc] initWithFrame:CGRectMake(0, self.heightHotNewsBarConstraint.constant/2 - 50/2, CGRectGetWidth(self.maqueeContainer.frame), 50) duration:70.0 andFadeLength:10.0f];
                hotNewsMaquee.text = [NSString stringWithFormat:@"       %@                            ",[defaults objectForKey:@"kmcMarquee"]] ;
                hotNewsMaquee.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
                hotNewsMaquee.textColor = [UIColor redColor];
                
                [self.maqueeContainer addSubview:hotNewsMaquee];
                
            }
        }else{
            if([defaults objectForKey:@"encMarquee"]){
                hotNewsMaquee = [[MarqueeLabel alloc] initWithFrame:CGRectMake(0, self.heightHotNewsBarConstraint.constant/2 - 50/2, CGRectGetWidth(self.maqueeContainer.frame), 50) duration:70.0 andFadeLength:10.0f];
                hotNewsMaquee.text = [NSString stringWithFormat:@"       %@                            ",[defaults objectForKey:@"encMarquee"]] ;
                hotNewsMaquee.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
                hotNewsMaquee.textColor = [UIColor redColor];
                
                [self.maqueeContainer addSubview:hotNewsMaquee];
            }
        }
    }else{
        NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:@"marquee"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [postSession cancel];
            
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getMarqueeText];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //NSLog(@"%@",payload);
                    
                    if([[defaults objectForKey:@"size"] integerValue] == 2){
                        //self.maqueeContainer.frame = CGRectMake(self.maqueeContainer.frame.origin.x, self.maqueeContainer.frame.origin.y, self.maqueeContainer.frame.size.width, self.maqueeContainer.frame.size.height + 10);
                        self.heightHotNewsBarConstraint.constant = 45;
                    }
                    
                    self.lblHotNewsTItle.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
                    
                    hotNewsMaquee = [[MarqueeLabel alloc] initWithFrame:CGRectMake(0, self.heightHotNewsBarConstraint.constant/2 - 50/2, CGRectGetWidth(self.maqueeContainer.frame), 50) duration:70.0 andFadeLength:10.0f];
                    hotNewsMaquee.text = [NSString stringWithFormat:@"       %@                            ",payload[@"result"][@"text"]] ;
                    hotNewsMaquee.font = [UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]];
                    hotNewsMaquee.textColor = [UIColor redColor];
                    
                    [self.maqueeContainer addSubview:hotNewsMaquee];
                    
                    if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
                        [defaults setObject:payload[@"result"][@"text"] forKey:@"kmcMarquee"];
                        [defaults synchronize];
                    }else{
                        [defaults setObject:payload[@"result"][@"text"] forKey:@"encMarquee"];
                        [defaults synchronize];
                    }
                    
                });
            }
            
        }];
        [postSession resume];
    }
    
}

- (void) getDrawerMenu{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
            if([defaults objectForKey:@"kmcDrawerMenu"]){
                drawer_categories = [[defaults objectForKey:@"kmcDrawerMenu"] mutableCopy];
                [self.tbMenuList reloadData];
            }
        }else{
            if([defaults objectForKey:@"encDrawerMenu"]){
                drawer_categories = [[defaults objectForKey:@"encDrawerMenu"] mutableCopy];
                [self.tbMenuList reloadData];
            }
        }
        
    }
    else{
        NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:@"drawer_menu"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [postSession cancel];
            
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getDrawerMenu];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    
                    drawer_categories = [[payload objectForKey:@"result"] mutableCopy];
                    [self.tbMenuList reloadData];
                    
                    if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
                        [defaults setObject:drawer_categories forKey:@"kmcDrawerMenu"];
                        [defaults synchronize];
                    }else{
                        [defaults setObject:drawer_categories forKey:@"encDrawerMenu"];
                        [defaults synchronize];
                    }
                    
                });
            }
            
        }];
        [postSession resume];
        
    }
    
    
}

- (void) getCategory{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
            if([defaults objectForKey:@"kmcMenu"]){
                
                isChangedLang = NO;
                items = [NSMutableArray new];
                categories = [[defaults objectForKey:@"kmcMenu"] mutableCopy];
                
                for (int i=0;i<categories.count;i++){
                    if(![[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                        if([categories[i][@"title"] isEqualToString:@"ទំព័រដើម"]){
                            //categories[i][@"title"] = @"Home";
                            [items addObject:@"Home"];
                        }else{
                            [items addObject:categories[i][@"title"]];
                        }
                    }else{
                        [items addObject:categories[i][@"title"]];
                    }
                }
                
                carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolbar delegate:self];
                [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.viewContainer];
                [carbonTabSwipeNavigation setNormalColor:[UIColor blackColor] font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                [carbonTabSwipeNavigation setSelectedColor:[UIColor colorWithRed:0/255.0f green:114/255.0f blue:188/255.0f alpha:1.0] font:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                
                self.loading.hidden = YES;
                
            }
        }else{
            if([defaults objectForKey:@"encMenu"]){
                
                isChangedLang = NO;
                items = [NSMutableArray new];
                categories = [[defaults objectForKey:@"encMenu"] mutableCopy];
                
                for (int i=0;i<categories.count;i++){
                    if(![[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                        if([categories[i][@"title"] isEqualToString:@"ទំព័រដើម"]){
                            //categories[i][@"title"] = @"Home";
                            [items addObject:@"Home"];
                        }else{
                            [items addObject:categories[i][@"title"]];
                        }
                    }else{
                        [items addObject:categories[i][@"title"]];
                    }
                }
                
                carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolbar delegate:self];
                [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.viewContainer];
                [carbonTabSwipeNavigation setNormalColor:[UIColor blackColor] font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                [carbonTabSwipeNavigation setSelectedColor:[UIColor colorWithRed:0/255.0f green:114/255.0f blue:188/255.0f alpha:1.0] font:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                
                self.loading.hidden = YES;
                
            }
        }
        
        
    }else{
        NSURLSessionDataTask *postSession = [[NSURLSession sharedSession] dataTaskWithRequest:[Common convertToGET:@"menu"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            [postSession cancel];
            
            if(error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCategory];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //NSLog(@"%@",payload);
                    
                    isChangedLang = NO;
                    
                    items = [NSMutableArray new];
                    categories = [[payload objectForKey:@"result"] mutableCopy];
                    
                    if([[defaults objectForKey:@"lang"] isEqualToString:@"km"]){
                        [defaults setObject:categories forKey:@"kmcMenu"];
                        [defaults synchronize];
                    }else{
                        [defaults setObject:categories forKey:@"encMenu"];
                        [defaults synchronize];
                    }
                    
                    for (int i=0;i<categories.count;i++){
                        if(![[defaults objectForKey:@"lang"]  isEqual: @"km"]){
                            if([categories[i][@"title"] isEqualToString:@"ទំព័រដើម"]){
                                //categories[i][@"title"] = @"Home";
                                [items addObject:@"Home"];
                            }else{
                                [items addObject:categories[i][@"title"]];
                            }
                        }else{
                            [items addObject:categories[i][@"title"]];
                        }
                    }
                    
                    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items toolBar:self.toolbar delegate:self];
                    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.viewContainer];
                    [carbonTabSwipeNavigation setNormalColor:[UIColor blackColor] font:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                    [carbonTabSwipeNavigation setSelectedColor:[UIColor colorWithRed:0/255.0f green:114/255.0f blue:188/255.0f alpha:1.0] font:[UIFont fontWithName:[Common getBoldFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
                    
                    self.loading.hidden = YES;
                    
                });
            }
            
        }];
        [postSession resume];
    }
    
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return drawer_categories.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(![[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        if([drawer_categories[indexPath.row][@"title"] isEqualToString:@"ទំព័រដើម"]){
            cell.lblTitle.text = @"Home";
        }else{
            cell.lblTitle.text = drawer_categories[indexPath.row][@"title"];
        }
    }else{
        cell.lblTitle.text = drawer_categories[indexPath.row][@"title"];
    }
    
    [cell.lblTitle setFont:[UIFont fontWithName:[Common getRegularFont] size:[appDelegate.rulesList[[[defaults objectForKey:@"size"] integerValue]][@"detail"] floatValue]]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //for(int i=0;i<categories.count;i++){
    if(![[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        if([drawer_categories[indexPath.row][@"cat_id"] intValue] == 113){
            [self performSegueWithIdentifier:@"settingSegue" sender:self];
            return;
        }else{
            [Common showAlert:@"នឹងមកដល់ឆាប់ៗ" title:@"BN Khmer" _self:self];
        }
    }else{
        if([drawer_categories[indexPath.row][@"cat_id"] intValue] == 120){
            [self performSegueWithIdentifier:@"settingSegue" sender:self];
            return;
        }else{
            [Common showAlert:@"នឹងមកដល់ឆាប់ៗ" title:@"BN Khmer" _self:self];
        }
    }
    
    // }
    //
    //    if(indexPath.row == 0){
    //        [carbonTabSwipeNavigation setCurrentTabIndex:0];
    //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    //        self.menuContainer.hidden = YES;
    //        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //
    //        return;
    //    }
    //    for(int i=0;i<items.count;i++){
    //        if([items[i] isEqualToString:categories[indexPath.row][@"title"]]){
    //            [carbonTabSwipeNavigation setCurrentTabIndex:i];
    //
    //            [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    //            self.menuContainer.hidden = YES;
    //            [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //
    //            break;
    //        }
    //    }
}

#pragma mark - CarbonKit
- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                         viewControllerAtIndex:(NSUInteger)index {
    
    NewsViewController *nv = [self.storyboard instantiateViewControllerWithIdentifier:@"newsView"];
    nv.catID = [categories[index][@"cat_id"] intValue];
    
    //DetailViewController *nv = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    
    return nv;
    
}

- (void)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation didMoveAtIndex:(NSUInteger)index{
    //self.title = items[index];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)menuClick:(id)sender {
    self.menuContainer.hidden = NO;
}
- (IBAction)clearMenuClick:(id)sender {
    self.menuContainer.hidden = YES;
}

- (IBAction)searchAction:(id)sender {
    [self performSegueWithIdentifier:@"searchSegue" sender:self];
    
}

- (IBAction)switchingEvent:(id)sender {
    
    if(!isChangedLang){
        isChangedLang = YES;
        
        self.loading.hidden = NO;
        
        if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
            
            [OneSignal sendTag:@"lang" value:@"en"];
            [defaults setObject:@"en" forKey:@"lang"];
            self.lblHotNewsTItle.text = @"  Latest";
            //self.title = @"Home";
            
        }else{
            
            [OneSignal sendTag:@"lang" value:@"km"];
            [defaults setObject:@"km" forKey:@"lang"];
            self.lblHotNewsTItle.text = @"  ថ្មីបំផុត";
            //self.title = @"ទំព័រដើម";
        }
        
        if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
            [self.btnLang setImage:[UIImage imageNamed:@"gb.png"] forState:UIControlStateNormal];
        }else{
            [self.btnLang setImage:[UIImage imageNamed:@"kh.png"] forState:UIControlStateNormal];
        }
        
        //[categories removeAllObjects];
        //categories = [NSMutableArray array];
        
        [[self.viewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [[self.maqueeContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [[self.toolbar subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            [self getDrawerMenu];
            [self getMarqueeText];
            [self getCategory];
        }else{
            [self getDrawerMenu];
            [self getMarqueeText];
            [self getAds];
        }
        
    }
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"detailSegue"]){
        UINavigationController *navController = [segue destinationViewController];
        DetailViewController *dv = (DetailViewController *)([navController viewControllers][0]);
        dv.isLocal = NO;
    }
}

- (void) orientationChanged:(NSNotification *)note
{
    hotNewsMaquee.frame = CGRectMake(0, self.heightHotNewsBarConstraint.constant/2 - 50/2, CGRectGetWidth(self.maqueeContainer.frame), 50);
    //    UIDevice * device = note.object;
    //    switch(device.orientation)
    //    {
    //        case UIDeviceOrientationPortrait:
    //            /* start special animation */
    //            break;
    //
    //        case UIDeviceOrientationPortraitUpsideDown:
    //            /* start special animation */
    //            break;
    //
    //        default:
    //            break;
    //    };
}

@end
