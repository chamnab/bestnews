//
//  DetailViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIScrollViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) BOOL isLocal;
@property (nonatomic) NSMutableDictionary *data;
@property (nonatomic) int catID;
@property (weak, nonatomic) IBOutlet UIView *loading;
- (IBAction)shareEvent:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;
@property (weak, nonatomic) IBOutlet UIView *vComment;
@property (weak, nonatomic) IBOutlet UIView *vFakeComment;
- (IBAction)closeCommentEvent:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vAd;

@end
