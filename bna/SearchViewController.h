//
//  SearchViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/28/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsViewController.h"

@interface SearchViewController : UIViewController <UISearchBarDelegate,NewsViewController>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@end

