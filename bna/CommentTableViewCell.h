//
//  CommentTableViewCell.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 11/4/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@end
