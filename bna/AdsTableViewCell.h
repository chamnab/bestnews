//
//  AdsTableViewCell.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/27/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdsTableViewCell;

@interface AdsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgAd;

@property (weak, nonatomic) IBOutlet UIButton *btnAd;
- (IBAction)openWeb:(id)sender;
@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic) id<AdsTableViewCell> delegate;

@end

@protocol AdsTableViewCell <NSObject>

- (void) openWeb;

@end
