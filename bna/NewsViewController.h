//
//  NewsViewController.h
//  bna
//
//  Created by Mr. Haruhiro Sato on 10/25/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsTableViewCell.h"
#import "AdsTableViewCell.h"

@protocol NewsViewController;

@interface NewsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,NewsTableViewCell,AdsTableViewCell>

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property int catID;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *loading;

@property (nonatomic) id<NewsViewController> delegate;

- (void) seachByText:(NSString *)text;

@end

@protocol NewsViewController <NSObject>

- (void) hideKeyboard;
- (void) scrollingEvent:(CGFloat)deltaY;
- (void) vDidLoad:(UITableView *)tableView;

@end
