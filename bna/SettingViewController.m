//
//  SettingViewController.m
//  bna
//
//  Created by Mr. Haruhiro Sato on 11/3/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import "SettingViewController.h"
#import "Common.h"
#import "AppDelegate.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont fontWithName:[Common getRegularFont] size:15.0f]};
    
    self.vNotifGroup.layer.borderColor = self.vFontGroup.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.vNotifGroup.layer.borderWidth = self.vFontGroup.layer.borderWidth = 1.0f;
    
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"size"] integerValue] == 0){
        [self.btnSmallFont setSelected:YES];
    }else if([[defaults objectForKey:@"size"] integerValue] == 1){
        [self.btnSmallFont.otherButtons[0] setSelected:YES];
    }else{
        [self.btnSmallFont.otherButtons[1] setSelected:YES];
    }
    
    if([[defaults objectForKey:@"noti"] integerValue] == 0){
        [self.btnOnNoti setSelected:YES];
    }else if([[defaults objectForKey:@"noti"] integerValue] == 1){
        [self.btnOnNoti.otherButtons[0] setSelected:YES];
    }
    
    if([[defaults objectForKey:@"lang"]  isEqual: @"km"]){
        
        self.lblFontDes.text = @"ទំហំតួរអក្សរ :";
        self.lblNotiDes.text = @"ការជូនដំណឹង :";
        [self.btnSmallFont setTitle:@"អក្សរតូច" forState:UIControlStateNormal];
        [self.btnSmallFont.otherButtons[0] setTitle:@"អក្សរធម្មតា" forState:UIControlStateNormal];
        [self.btnSmallFont.otherButtons[1] setTitle:@"អក្សរធំ" forState:UIControlStateNormal];
        
        [self.btnOnNoti setTitle:@"បើក" forState:UIControlStateNormal];
        [self.btnOnNoti.otherButtons[0] setTitle:@"បិទ" forState:UIControlStateNormal];
    }else{
        [self.btnSmallFont setTitle:@"Small" forState:UIControlStateNormal];
        [self.btnSmallFont.otherButtons[0] setTitle:@"Medium" forState:UIControlStateNormal];
        [self.btnSmallFont.otherButtons[1] setTitle:@"Big" forState:UIControlStateNormal];
        
        [self.btnOnNoti setTitle:@"On" forState:UIControlStateNormal];
        [self.btnOnNoti.otherButtons[0] setTitle:@"Off" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)fontSizeEvent:(DLRadioButton *)sender {
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInteger:sender.selectedButton.tag] forKey:@"size"];
    [defaults setObject:@1 forKey:@"fontReset"];
    [defaults synchronize];
    //NSLog(@"%ld is selected.\n", [[defaults objectForKey:@"size"] integerValue]);
}

- (IBAction)notiEvent:(DLRadioButton *)sender {
    
    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInteger:sender.selectedButton.tag] forKey:@"noti"];
    [defaults synchronize];

    if(sender.selectedButton.tag == 0){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else{
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
    
}
@end
