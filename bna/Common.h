//
//  Common.h
//  WatamiTagcast
//
//  Created by Mr. Haruhiro Sato on 1/22/16.
//  Copyright © 2016 Choung Chamnab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Common : NSObject

+ (BOOL) isIphone4;
+ (BOOL) isIphone6;
+ (BOOL) isIphone6P;
+ (BOOL) isBiggerThaniPhone5;
+ (NSString *) getToken;
+ (NSString *) getLang;
+ (void) showAlert:(NSString *)msg title:(NSString *)msg _self:(UIViewController *)_self;
+ (UIAlertController *) getRetryAlert;
+ (NSURL *) getBaseURL;
+ (NSMutableURLRequest *)convertToPOST:(NSMutableDictionary*)data route:(NSString *)route;
+ (NSMutableURLRequest *)convertToGET:(NSString *)route;
+ (void) changeNavigationColor:(UIViewController *)cSelf;
+ (NSString *) getLocaleText:(NSString*)code;
+ (void) trackScreen:(NSString *) name;
+ (void) trackEvent:(NSString *) action;
+ (void) changeFont:(NSArray *)labelList fontSize:(CGFloat)fontSize;
+ (void) changeNavigationFont:(UIViewController *) _self;
+ (NSMutableArray *) getTagCastList;
+ (void) updateTagCastList:(NSMutableArray *)tagCastList;
+ (CGRect) getTextHeight:(NSString *)text font:(UIFont *)font width:(CGFloat) width;
+ (UIColor *) getColor;
+ (NSString *) getRegularFont;
+ (NSString *) getBoldFont;
+ (NSString *) stringByStrippingHTML:(NSString *)s;
+ (NSURL *) encodedURL:(NSString *)url;
+ (NSString *) randomStringWithLength: (int) len;
+ (NSString *) convertDate:(NSString *)date;
+ (NSMutableDictionary *) getMonthList;
+ (NSMutableDictionary *) getDayList;
@end
